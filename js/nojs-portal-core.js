
/* ========================================================================
 * Terramagnet-Developer: core.js v1.0.0-SNAPSHOT
 * http://open.developer.terramangnet.org/javascript/core
 * ========================================================================
 * Copyright 2016-2020 Terramagnet, Inc.
 * Licensed under MIT (https://opensource.org/licenses/MIT)
 * ======================================================================== */
+function($) {
    var ctx = "/cloud-portal/dev";
    var CONTROLLERS = {};

    Object.prototype.path = function (String path) {
        var obj = this;
        $.each(path.split("."), function () {
            obj = obj[this];
            if (obj === undefined) {
                return false;
            }
        });
        return obj;
    }
    var Contrller = function (element, options) {
        this.$element = $(element);
        this.options = $.extend({}, Contrller.DEFAULTS, options);
        this.name = null;
        this.params = {};

        var controller = this;
        $.each(element.attributes, function () {
            var attrName = this.name;
            var r = /data-param-(.*)/.exec(attrName);
            if (r) {
                controller.params[r[1]] = this.value;
            }
        });

        this.data = null;

        this.actions = {
            "iterators" : []
        };
    };

    Contrller.VERSION = '0.1.0';
    Contrller.DEFAULTS = {
        "loading" : $("<img src=\"images/loading.gif\" alt=\"loading...\""),
        "successCode" : 0,

        "cache" : false,
        "method" : "GET"
    };

    Contrller.prototype.init = Contrller.prototype.load = function () {
        if (this.url) {
            $.ajax(ctx + this.url, {
                "type" : this.options.method,
                "cache" : this.cache(),
                "context" : this,
                "data" : this.params,
                "dataType" : "json"
            }).done(function (data, textStatus, jqXHR) {
                if (data.code !== this.options.successCode) {
                    if (data.message) {
                        $nojs.modal.error(data.message);
                    }
                    return;
                }
                this.data(data);
                this.bind();
            }).fail(function (jqXHR, textStatus, errorThrown) {
                $nojs.modal.error("系统繁忙，请稍候再试~~");
            });
        }
    };

    Contrller.prototype.data = function (data) {
        if (!data) { //getter
            return this.data;
        } else { //setter
            this.data = data;
        }
    }

    Contrller.prototype.bind = function () {
        this._bindData(this.data());

        IteratorPlugin.call($("[data-iterator]", this.$element), this);
    }

    /**
     * 绑定含有 data-bind 属性的字段.
     * @param  {[type]} data [模型数据]
     */
    Contrller.prototype._bindData = function (data) {
        var controller = this;
        var _inputCache = {};
        $("[data-bind]", controller.$element).each(function () {
            var $dom = $(this);
            if ($dom.is("input") && $dom.attr("name")) {
                if (!_inputCache[$dom.attr("name")]) {
                    _inputCache[$dom.attr("name")] = $("input[name=" + $nojs._replaceMetacharator($dom.attr("name")) + "]", controller.$element);
                    $dom.val(data.path($dom.attr("data-bind") || $dom.attr("name")));
                }
            } else {
                $dom.vald(data.path($dom.attr("data-bind")));
            }
        });
    }

    Contrller.prototype.cache = function (cachable) {
        if (cachable === undefined) { //getter
            var storageCacheFlag = $.sessionStorage.get[this.name, "cache"];
            if (storageCacheFlag === undefined || storageCacheFlag === null) {
                return this.options.cache;
            }
            return storageCacheFlag;
        } else { //setter
            $.sessionStorage.set[this.name, "cache", cachable];
        }
    }

    Iterator.DEFAULTS = {
        "var" : null,
        "status":"i"
    };
    var Iterator = function (element, prot, options) {
        this.$element = $(element);
        this.ctrlName = prot.ctrlName;
        this.name = prot.name;

        this.options = options;
    };
    Iterator.prototype.data = Contrller.prototype.data;
    Iterator.prototype._bindData = Contrller.prototype._bindData;
    Iterator.prototype.init = function () {
        var controllerData = CONTROLLERS[ctrlName].data();
        var data = controllerData.path(this.options.name);
        this.data(data);

        $.each(data, function (k, v) {
            this.$element.before(this.$element.clone());

            //TODO binding data
        });
        this.$element.remove();
    }

    function Plugin(option) {
        return this.each(function () {
            var $this = $(this);
            var ctrlName = $this.attr("data-ctrl");
            if (!ctrlName) {
                return;
            }
            var data = $this.data('nojs.dev.controller');

            if (!data) {
                if (CONTROLLERS[ctrlName]) {
                    $this.data('nojs.dev.controller', (data = CONTROLLERS[ctrlName]));
                    data.$element.push(this);
                } else {
                    $this.data('nojs.dev.controller', (data = new Contrller(this, $this.data())));
                    data.name = ctrlName;
                    CONTROLLERS[ctrlName] = data;
                }
            }

            if (!option) {
                data.init();
            } else if (typeof option == "string" && $.isFunction(data[option])) {
                data[option]();
            }
        });
    };
    function IteratorPlugin(controller) {
        return this.each(function () {
            var $this = $(this);
            var iteratorRegexpResult = /(\w+)\s+in\s+(\w+)/.exec($this.attr("data-iterator"));
            if (!iteratorRegexpResult) {
                return;
            }
            var elemName = iteratorRegexpResult[1];
            var actionName = iteratorRegexpResult[2];

            var data = new Iterator(this, {
                    "ctrlName" : controller.name,
                    "name" : actionName
                }, $.extend({"var" : elemName}, $this.data()));
            controller.actions.iterators.push(data);
        });
    }

    var old = $.fn.controller;

    $.fn.controller = Plugin;
    $.fn.controller.Constructor = Contrller;

    // CONTRLLER NO CONFLICT
    // ==================

    $.fn.controller.noConflict = function () {
        $.fn.controller = old;
        return this;
    };

    // CONTRLLER DATA-API
    // ===============
    $(document).ready(function () {
        Plugin.call($("[data-ctrl]"));
    });
}
(jQuery);
