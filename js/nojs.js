﻿(function (window, $) {
    window.console = console || {};
    console.debug = console.debug || function () {};

    var event = {
        before: [],
        ready: []
    };
    var $nojs = function (context) {
        if ($.onApplicationStart && $.isFunction($.onApplicationStart)) {
            $.onApplicationStart();
        }
        for (var i in event.before) {
            if ($.isFunction(event.before[i])) {
                if (event.before[i].call($nojs, context) === false) {
                    return;
                }
            }
        }
        for (var name in $nojs) {
            var app = $nojs[name];
            if ($.isFunction(app.enable) ? app.enable() : app.enable === true) {
                if (app.singleton && app.executed) {
                    continue;
                }
                if ($nojs.debug) {
                    console.debug("enable " + name + " ...");
                }
                app.init(context);
                if ($nojs.debug) {
                    console.debug("....................... sucess !");
                }

                if (app.singleton) {
                    app.executed = true;
                }
            }
        }
        for (var i in event.ready) {
            $.isFunction(event.ready[i]) && event.ready[i].call($nojs, context);
        }
    };
    $nojs.multipartModule = {
        enable: true,
        init: function (context) {
            for (var name in this) {
                var module = this[name];
                if (module && $.isFunction(module.init) && ($.isFunction(module.enable) ? module.enable() : module.enable === true)) {
                    module.init(context, this);
                    if ($nojs.debug) {
                        console.debug("submodule " + name + " enabled!");
                    }
                }
            }
        },
        addModule: function (name, module) {
            if (module && module.init) {
                this[name] = module;
            }
        },
        removeModule: function (name) {
            this[name] = undefined;
        }
    };

    $nojs.before = function (func) {
        event.before.push(func);
    };
    $nojs.ready = function (func) {
        event.ready.push(func);
    };


    $nojs.checkboxTogether = {
        enable: true,
        selector: "input:checkbox[data-member]",
        $member: function ($leader, context) {
            var memberName = $leader.data("member");
            return $("input:checkbox[name=" + memberName + "]:not(:disabled)", context);
        },
        render: function ($leader, $member, context) {
            var $render = $leader.data("$render");
            if ($render === false) {
                return;
            }
            if (!$render) {
                var memberName = $leader.data("member");
                $render = $(":button[data-checkbox-require=" + memberName + "]");
                if ($render.length === 0) {
                    $leader.data("$render", false);
                    return;
                } else {
                    $leader.data("$render", $render);
                }
            }
            $render.prop("disabled", $member.filter(":checked").length === 0);
        },
        init: function (context) {
            var module = this;
            $(module.selector, context).each(function () {
                var $leader = $(this);
                var $member = module.$member($leader, context);
                $leader.change(function () {
                    $member.prop("checked", $leader.prop("checked"));
                    module.render($leader, $member, context);
                });
                $member.change(function () {
                    $leader.prop("checked", $member.length === $member.filter(":checked").length);
                    module.render($leader, $member, context);
                });
                module.render($leader, $member, context);
            });
        }
    };
    $nojs.readonly = {
        enable: true,
        selector: ".readonly",
        init: function (context) {
            var $readonlyContext = $(this.selector, context);
            $readonlyContext.find(":input").prop("readonly", true);
            $readonlyContext.find("textarea").each(function () {
                var $textarea = $(this);
                if ($textarea.hasClass("editor")) {
                    var text = $textarea.text();
                    $textarea.after(text);
                    $textarea.hide();
                } else {
                    $textarea.prop("readonly", true);
                }
            });
            $readonlyContext.find(":radio,:checkbox,select").prop("disabled", true);
            $readonlyContext.find(":button:not(.cancel)").hide();
        }
    };
    $nojs.selectValueInitialization = {//select值绑定
        enable: true,
        selector: "select[data-value]",
        event: "$nojs-select-initialize",
        init: function (context) {
            $(this.selector, context).on(this.event, function (event) {
                var $select = $(this);
                var value = $select.data("value");
                $select.children("option:selected").prop("selected", false);
                value && $select.children("option[value=" + value + "]").prop("selected", true);
                var autofire = $select.data("autofire");
                if (autofire) {
                    $nojs.ready(function () {
                        $select.change();
                    });
                }
            }).trigger(this.event);
        }
    };
    $nojs.radioGroupValueInitialization = {////radio值绑定
        enable: true,
        selector: ".radio-group",
        event: "$nojs-radio-initialize",
        init: function (context) {
            $(this.selector, context).on(this.event, function (event) {
                var $group = $(this);
                var value = $group.data("value");
                if (value !== undefined) {
                    $group.find(":radio:checked")
                        .prop("checked", false)
                        .closest(".btn")
                        .removeClass("active");
                    $group.find(":radio[value=\"" + value + "\"]")
                        .prop("checked", true)
                        .closest(".btn")
                        .addClass("active");
                }
                var autofire = $group.data("autofire");
                if (autofire) {
                    $group.change();
                }
            }).trigger(this.event);
        }
    };
    $nojs.trimText = {
        enable: true,
        selector: ":text:not([data-skip-trim])",
        init: function (context) {
            $(this.selector, context).change(function () {
                this.value = $.trim(this.value);
            });
        }
    };
    $nojs.resetLookup = {//重置表单
        enable: true,
        selector: {
            reset: ".reset",
            control: ":input",
            form: "form.lookup,form.app-lookup-form"
        },
        init: function (context) {
            var $lookupForm = $(this.selector.form, context);
            var controlSelector = this.selector.control;
            $(this.selector.reset, $lookupForm).click(function () {
                $(controlSelector, $lookupForm).val("");
                $lookupForm.submit();
            });
        }
    };
    $nojs.closeable = {
        enable: true,
        init: function (context) {
            $(document).on("click", "button.close", function (e) {
                if (e.isDefaultPrevented()) {
                    return false;
                }
                $(this).closest("[data-closeable]").remove();
            });
        }
    };

    //Copy from jQuery.
    window.$nojs = $nojs;
    if (typeof define === "function" && define.amd && define.amd.$nojs) {
        define("$nojs", [], function () {
            return $nojs;
        });
    }

    $.fn.extend({
        "vald": function () {
            if (arguments.length) {
                _setValue(this, arguments[0]);
                return this;
            } else {
                return _getValue(this);
            }
            function _getValue($j) {
                if ($j.is(":input")) {
                    return $j.val();
                } else if ($j.is("img")) {
                    return $j.attr("src");
                } else {
                    return $j.html();
                }
            }

            function _setValue($j, value) {
                if ($j.is(":input")) {
                    $j.val(value);
                } else if ($j.is("img")) {
                    $j.attr("src", value);
                } else {
                    $j.html(value);
                }
            }
        },
        "pushData": function (name, value) {
            var data = this.data(name);
            if (!data) {
                data = [];
                this.data(name, data);
            }
            $.isArray(data) && (value || value === false) && data.push(value);
        },
        "dataExcept": function () {
            var data = $.extend({}, this.data());
            for (var i = 0; i < arguments.length; i++) {
                delete data[arguments[i]];
            }
            return data;
        },
        "dataWith": function () {
            var attrs = this[0].attributes;
            var data = {};

            for (var k = 0; k < attrs.length; k++) {
                var attr = attrs[k];
                var attrName = attr.name;
                if (attrName.indexOf("data-") === -1) {
                    continue;
                }

                for (var i = 0; i < arguments.length; i++) {
                    var namespace = arguments[i].toString();
                    if (attrName.indexOf("data-" + namespace) !== -1) {
                        var name = attrName.slice(namespace.length + 6)
                            .replace(/-([a-z])/g, function (g) {
                                return g[1].toUpperCase();
                            });

                        data[name] = attr.value;
                        break;
                    }
                }
            }

            return data;
        }
    });
})(window, jQuery);

$(document).ready(function () {
    window.$nojs(document);
});